=begin
Copyright 2007-2008 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end
class Preference
	attr_accessor :dir #Array -> root directories for explorer
	attr_accessor :radio #Hash -> Name and url for webradios
	attr_accessor :gui #Hash -> Graphical preferences
	attr_accessor :player #Hash -> Player preferences
	attr_accessor :ext #Hash -> Supported extensions
	def initialize
		@player={}
		@gui={}
		@radio={}
		@dir=[]
		@ext={'wav'=>false,'ogg'=>false,'flac'=>false,'mp3'=>false,'wma'=>false,'m3u'=>false,'pls'=>false}
		@file=File.join(Home,'ZiK.ini')
		
#*******Default values**********
@player['current']=0
@player['volume']=100
@player['shuffle']=false
@player['repeat']=false
@player['playing']=false
@gui['xposition']=80
@gui['yposition']=80
@gui['width']=500
@gui['height']=600
@gui['paned_position']=200
@gui['showexplo']=true
@gui['showlist']=true
		
		if File.readable?(@file)
			read
			puts "Loading preferences"
		else
			$stderr.puts "Can\'t load preferences"
		end
	end
	
	def read
		f=File.open(@file,'r')
		while (line=f.gets)
			if line.chomp=='[player]'
				line=f.gets
				while line !~ /^\[/ and !line.nil? #while line do not begin with [
					value=f.gets
					@player[line.chomp!]=value.chomp!
					line=f.gets
				end
			end
			if line.chomp=='[gui]'
				line=f.gets
				while line !~ /^\[/ and !line.nil? #while line do not begin with [
					value=f.gets
					@gui[line.chomp!]=value.chomp!
					line=f.gets
				end
			end
			if line.chomp=='[directory]'
				i=0
				line=f.gets
				while line !~ /^\[/ and !line.nil? #while line do not begin with [
					@dir[i]=line.chomp!;i+=1
					line=f.gets
				end
			end
			if line.chomp=='[radio]'
				line=f.gets
				while line !~ /^\[/ and !line.nil? #while line do not begin with [
					temp=f.gets
					@radio[line.chomp!]=temp.chomp!
					line=f.gets
				end
			end
			if  line.chomp=='[ext]'
				line=f.gets
				while line !~ /^\[/ and !line.nil? #while line do not begin with [
					@ext[line.chomp!]=true
					line=f.gets
				end
			end
		end
		f.close
		str2value
	end
	
	def write
		begin
			puts "Writting preferences"
			f=File.open(@file,'w')
			
			f.puts '[player]'
			f.puts 'current';f.puts @player['current']
			f.puts 'volume';f.puts @player['volume']
			f.puts 'shuffle';f.puts @player['shuffle']
			f.puts 'repeat';f.puts @player['repeat']
			f.puts 'playonstart';f.puts @player['playonstart']
			
			f.puts '[gui]'
			f.puts 'xposition';f.puts @gui['xposition']
			f.puts 'yposition';f.puts @gui['yposition']
			f.puts 'width';f.puts @gui['width']
			f.puts 'height';f.puts @gui['height']
			f.puts 'paned_position';f.puts @gui['paned_position']
			f.puts 'showexplo';f.puts @gui['showexplo']
			f.puts 'showlist';f.puts @gui['showlist']
			f.puts 'tray';f.puts @gui['tray']
			
			f.puts '[directory]'
			f.puts @dir
			
			f.puts '[radio]'
			@radio.each{|key,value|
				f.puts key
				f.puts value
				}
			
			f.puts '[ext]'
			@ext.each{|ext,show|
				f.puts ext if show
			}
		rescue
			$stderr.puts "Can\'t save preferences"
		ensure
			if !f.nil?
				f.close
			end
		end
	end
	
	private
#*********************Change the format of the value from string************
	def str2value
		@player['current']=@player['current'].to_i
		@player['volume']=@player['volume'].to_f
		if @player['shuffle']=='false'
			@player['shuffle']=false
		else
			@player['shuffle']=true
		end
		if @player['repeat']=='false'
			@player['repeat']=false
		else
			@player['repeat']=true
		end
		if @player['playonstart']=='true'
			@player['playonstart']=true
		else
			@player['playonstart']=false
		end
		
		@gui['xposition']=@gui['xposition'].to_i
		@gui['yposition']=@gui['yposition'].to_i
		@gui['width']=@gui['width'].to_i
		@gui['height']=@gui['height'].to_i
		@gui['paned_position']=@gui['paned_position'].to_i
		if @gui['showexplo']=='false'
			@gui['showexplo']=false
		else
			@gui['showlist']=true
		end
		if @gui['showlist']=='false'
			@gui['showlist']=false
		else
			@gui['showlist']=true
		end
		if @gui['tray']=='false'
			@gui['tray']=false
		else
			@gui['tray']=true
		end
	end
end
