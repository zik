=begin
Copyright 2007-2008 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

class Gui < Gtk::Window
	include GetText
	bindtextdomain('ZiK',{:path=>Ldir})
	
	include ReadPlaylist
	
	def initialize
####################################
#-------Preferences-----------------
####################################
		@pref=Preference.new
		
		@border_width=10
		@showlistn=1
####################################
#-----------Gstreamer player--------
####################################
		@list1=Playlist.new(self,true)
		@player=Player.new(@list1,@pref.player)
		@player.volume=@pref.player['volume']/100
##################
#-------Gui-------
##################
		@seeking=false
#*****************Explorer & list widgets****************
		###---Browse--------
		@treestore=Tree.new(String,String,String)
		@path_explo=@treestore.refresh(@pref.dir,@pref.ext)
		
		@iadd2plist_tree=Gtk::Image.new(Gtk::Stock::GO_FORWARD,Gtk::IconSize::MENU)
		@badd2plist_tree=Gtk::ImageMenuItem.new(_('Add to playlist'))
		@badd2plist_tree.image=@iadd2plist_tree
		@badd2plist_tree.signal_connect("activate") {
			case @showlistn
			when 1
				@list1.add(@tree_select)
				@player.add
			when 2
				@list2.add(@tree_select)
			end
		}
		@sep_tree=Gtk::SeparatorMenuItem.new
		@brefresh_tree=Gtk::ImageMenuItem.new(Gtk::Stock::REFRESH)
		@brefresh_tree.signal_connect("activate") {@path_explo=@treestore.refresh(@pref.dir,@pref.ext)}
		@badd_tree=Gtk::ImageMenuItem.new(Gtk::Stock::ADD)#Optimiser le code. -> ne pas rafraichir mais ajouter les nouveaux élements.
		@badd_tree.signal_connect("activate") {
			dialog=Gtk::FileChooserDialog.new(_("Add a directory"), self,
				Gtk::FileChooser::ACTION_SELECT_FOLDER, nil,
				[Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL], [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
			if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
				@pref.dir.push(dialog.filename)
				@path_explo=@treestore.refresh(@pref.dir,@pref.ext)
			end
			dialog.destroy
		}
		@brm_tree=Gtk::ImageMenuItem.new(Gtk::Stock::REMOVE)
		@brm_tree.signal_connect("activate") {
			rm=false; collect=[]
			@tree_select.selected_each{|model,path,iter|
				if path.depth==1
					collect.push(path.to_s.to_i)
					rm=true
				end
			}
			if rm
				collect.sort!; a=0
				collect.each{|i| @pref.dir.delete_at(i-a);a+=1}
				@path_explo=@treestore.refresh(@pref.dir,@pref.ext)
			else
				puts "Delete only root directories"
			end
		}
		@bclear_tree=Gtk::ImageMenuItem.new(Gtk::Stock::CLEAR)
		@bclear_tree.signal_connect("activate") {@pref.dir.clear; @treestore.clear}
		@bpref_tree=Gtk::ImageMenuItem.new(Gtk::Stock::PREFERENCES)
		@bpref_tree.signal_connect("activate") {
			dialog=Option.new(self,@pref.dir,@pref.ext,@border_width)
			if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
				@pref.dir=dialog.dir
				@pref.ext=dialog.ext
				@path_explo=@treestore.refresh(@pref.dir,@pref.ext)
			end  
			dialog.destroy
		}
		@popup_tree=Gtk::Menu.new
		@popup_tree.append(@badd2plist_tree)
		@popup_tree.append(@sep_tree)
		@popup_tree.append(@brefresh_tree)
		@popup_tree.append(@badd_tree)
		@popup_tree.append(@brm_tree)
		@popup_tree.append(@bclear_tree)
		@popup_tree.append(@bpref_tree)
		@popup_tree.show_all
		
		@treeview=Gtk::TreeView.new(@treestore)
		@tree_select=@treeview.selection
		@tree_select.mode = Gtk::SELECTION_MULTIPLE
		@renderer = Gtk::CellRendererText.new
		@col = Gtk::TreeViewColumn.new(_("Browse"), @renderer, :text => 0)
		@treeview.append_column(@col)
		
		@treeview.signal_connect("button_press_event"){|widget, event|
			if event.button == 3
				@popup_tree.popup(nil, nil, event.button, event.time)
			end
		}
		
		@wtree=Gtk::ScrolledWindow.new
		@wtree.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
		@wtree.add(@treeview)
		###---Buttons---------
		@badd=Gtk::Button.new
		@badd.add(Gtk::Image.new(Gtk::Stock::GO_FORWARD,Gtk::IconSize::BUTTON))
		@badd.signal_connect('clicked') {
		case @showlistn
		when 1
			@list1.add(@tree_select)
			@player.add
		when 2
			@list2.add(@tree_select)
		when 3
			add_radio
		end
		}
		@bdel=Gtk::Button.new
		@bdel.add(Gtk::Image.new(Gtk::Stock::GO_BACK,Gtk::IconSize::BUTTON))
		@bdel.signal_connect('clicked') {
		case @showlistn
		when 1
			sel=@list1.del(@list1_select)
			@player.del(sel)
			current_shuffle=@player.order[@player.current]
			name=@list1.name[current_shuffle]
			duration=(@list1.duration[current_shuffle].to_i)
			@stitle.set_markup("<b>#{name}</b>")
			if duration <= 0
				show_duration=false
				@lduration.text='--:--'
			else
				show_duration=true
				@barpos.set_range(0,duration)
				duration_s=(Time.at(duration).strftime("%M:%S"))
				@lduration.text=duration_s
			end
			@list1.store.each{|model,path,iter|
				if iter[0]==name
					iter[1]=true
				else
					iter[1]=false
				end
			}
		when 2
			@list2.del(@list2_select)
		when 3
			rm_radio
		end
		}
		@bclear=Gtk::Button.new
		@bclear.add(Gtk::Image.new(Gtk::Stock::CLEAR,Gtk::IconSize::BUTTON))
		@bclear.signal_connect('clicked') {
		case @showlistn
		when 1
			@player.stop
			@player.del_all
			@list1.del_all
		when 2
			@list2.del_all
		when 3
			@player.stop
			@pref.radio.clear
			@radiostore.clear
		end
		}
		###---Lists-------------
		#choose the list
		@combolist=Gtk::ComboBox.new
		@combolist.append_text(_('Listen to playlist'))
		@combolist.append_text(_('Manage a secondary list'))
		@combolist.append_text(_('Listen to radio'))
		#@combolist.append_text(_('Lire un cd'))
		@combolist.active=0
		@combolist.signal_connect('changed'){
		case @combolist.active
		when 0
			@wradio.hide;@wlist2.hide;@mbox.hide;@wcd.hide;@wlist1.show;@showlistn=1
		when 1
			@wradio.hide;@wlist1.hide;@wcd.hide;@wlist2.show;@mbox.show;@showlistn=2
		when 2
			@wlist1.hide;@wlist2.hide;@mbox.hide;@wcd.hide;@wradio.show;@showlistn=3
		when 3
			@wlist1.hide;@wlist2.hide;@mbox.hide;@wradio.hide;@wcd.show;@showlistn=4
		end
		}
		
		#List 1
		#@list1=Playlist.new(self,true)  Already done
		
		@irm_list1=Gtk::Image.new(Gtk::Stock::GO_BACK,Gtk::IconSize::MENU)
		@brm_list1=Gtk::ImageMenuItem.new(_('Delete'))
		@brm_list1.image=@irm_list1
		@brm_list1.signal_connect("activate") {
			sel=@list1.del(@list1_select)
			@player.del(sel)
		}
		@brefresh_list1=Gtk::ImageMenuItem.new(Gtk::Stock::REFRESH)
		@brefresh_list1.signal_connect("activate") {@list1.refresh}
		@bexport_list1=Gtk::ImageMenuItem.new(Gtk::Stock::SAVE)
		@bexport_list1.signal_connect("activate") {@list1.export(self)}
		@bimport_list1=Gtk::ImageMenuItem.new(Gtk::Stock::ADD)
		@bimport_list1.signal_connect("activate") {@list1.import(self);@player.add}
		@bclear_list1=Gtk::ImageMenuItem.new(Gtk::Stock::CLEAR)
		@bclear_list1.signal_connect("activate") {
			@player.stop unless @player.radio?
			@player.del_all
			@list1.del_all
		}
		@bsearch_list1=Gtk::ImageMenuItem.new(Gtk::Stock::FIND)
		@bsearch_list1.signal_connect("activate"){
			@combolist.active=0
			@wsearch_plist=Search::List.new unless Search.plist?
			position=self.position+@boxlist.allocation.to_a
			@wsearch_plist.search(position,@list1,@listview1)
		}
		@sep_list1=Gtk::SeparatorMenuItem.new
		@bedit_list1=Gtk::ImageMenuItem.new(Gtk::Stock::EDIT)
		@bedit_list1.signal_connect("activate") {EditTag.new(self,@list1_select,@list1,@border_width)}
		@popup_list1=Gtk::Menu.new
		@popup_list1.append(@brm_list1)
		@popup_list1.append(@brefresh_list1)
		@popup_list1.append(@bexport_list1)
		@popup_list1.append(@bimport_list1)	
		@popup_list1.append(@bclear_list1)
		@popup_list1.append(@bsearch_list1)
		@popup_list1.append(@sep_list1)
		@popup_list1.append(@bedit_list1)
		@popup_list1.show_all
		
		@listview1=Gtk::TreeView.new(@list1.store)
		@listview1.headers_visible=false
		@list1_select=@listview1.selection
		@list1_select.mode = Gtk::SELECTION_MULTIPLE
		@renderer=Gtk::CellRendererText.new
		#@renderer.weight=Pango::WEIGHT_BOLD
		#@col=Gtk::TreeViewColumn.new("Liste de lecture", @renderer, :text => 0, :weight_set => 1)
		@renderer.background='Pink'#cf:/etc/X11/rgb.txt
		@col=Gtk::TreeViewColumn.new("Liste de lecture", @renderer, :text => 0, :background_set => 1)
		@listview1.append_column(@col)
		
		@listview1.signal_connect("row-activated"){|view, path, column|
			@player.stop
			play(path.to_s.to_i)
			}
		@listview1.signal_connect("button_press_event"){|widget, event|
			if event.button == 3
				@popup_list1.popup(nil, nil, event.button, event.time)
			end
		}
		
		@wlist1=Gtk::ScrolledWindow.new
		@wlist1.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
		@wlist1.add(@listview1)
		
		#List 2
		@list2=Playlist.new(self)
		
		@irm_list2=Gtk::Image.new(Gtk::Stock::GO_BACK,Gtk::IconSize::MENU)
		@brm_list2=Gtk::ImageMenuItem.new(_('Delete'))
		@brm_list2.image=@irm_list2
		@brm_list2.signal_connect("activate") {
			sel=@list2.del(@list2_select)
			@player.del(sel)
		}
		@brefresh_list2=Gtk::ImageMenuItem.new(Gtk::Stock::REFRESH)
		@brefresh_list2.signal_connect("activate") {@list2.refresh}
		@bexport_list2=Gtk::ImageMenuItem.new(Gtk::Stock::SAVE)
		@bexport_list2.signal_connect("activate") {@list2.export(self)}
		@bimport_list2=Gtk::ImageMenuItem.new(Gtk::Stock::ADD)
		@bimport_list2.signal_connect("activate") {@list2.import(self);@player.add}
		@bclear_list2=Gtk::ImageMenuItem.new(Gtk::Stock::CLEAR)
		@bclear_list2.signal_connect("activate") {
			@player.stop unless @player.radio?
			@player.del_all
			@list2.del_all
		}
		@sep_list2=Gtk::SeparatorMenuItem.new
		@bedit_list2=Gtk::ImageMenuItem.new(Gtk::Stock::EDIT)
		@bedit_list2.signal_connect("activate") {EditTag.new(self,@list2_select,@list2,@border_width)}
		@popup_list2=Gtk::Menu.new
		@popup_list2.append(@brm_list2)
		@popup_list2.append(@brefresh_list2)
		@popup_list2.append(@bexport_list2)
		@popup_list2.append(@bimport_list2)	
		@popup_list2.append(@bclear_list2)
		@popup_list2.append(@sep_list2)
		@popup_list2.append(@bedit_list2)
		@popup_list2.show_all
	
		@listview2=Gtk::TreeView.new(@list2.store)
		@listview2.headers_visible=false
		@list2_select=@listview2.selection
		@list2_select.mode = Gtk::SELECTION_MULTIPLE
		@renderer = Gtk::CellRendererText.new
		@col=Gtk::TreeViewColumn.new("Gérer une liste", @renderer, :text => 0)
		@listview2.append_column(@col)
		
		@listview2.signal_connect("button_press_event"){|widget, event|
			if event.button == 3
				@popup_list2.popup(nil, nil, event.button, event.time)
			end
		}
		
		@wlist2=Gtk::ScrolledWindow.new
		@wlist2.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
		@wlist2.add(@listview2)
		
		#Buttons
		@iup=Gtk::Image.new(Gtk::Stock::GO_UP,Gtk::IconSize::BUTTON)
		@bup=Gtk::Button.new
		@bup.image=@iup
		@bup.signal_connect('clicked') {@list2.up(@list2_select)}
		@idown=Gtk::Image.new(Gtk::Stock::GO_DOWN,Gtk::IconSize::BUTTON)
		@bdown=Gtk::Button.new
		@bdown.image=@idown
		@bdown.signal_connect('clicked') {@list2.down(@list2_select)}
		@iexplo2m=Gtk::Image.new(Gtk::Stock::ADD,Gtk::IconSize::BUTTON)
		@bexplo2m=Gtk::Button.new
		@bexplo2m.image=@iexplo2m
		@bexplo2m.signal_connect('clicked') {@list2.import(self)}
		@im2explo=Gtk::Image.new(Gtk::Stock::SAVE,Gtk::IconSize::BUTTON)
		@bm2explo=Gtk::Button.new
		@bm2explo.image=@im2explo
		@bm2explo.signal_connect('clicked') {@list2.export(self)}
		@iplist2m=Gtk::Image.new(Gtk::Stock::COPY,Gtk::IconSize::BUTTON)
		@bplist2m=Gtk::Button.new
		@bplist2m.image=@iplist2m
		@bplist2m.signal_connect('clicked') {@list2.copy(@list1)}
		@im2plist=Gtk::Image.new(Gtk::Stock::PASTE,Gtk::IconSize::BUTTON)
		@bm2plist=Gtk::Button.new
		@bm2plist.image=@im2plist
		@bm2plist.signal_connect('clicked') {
			@list1.copy(@list2)
			@player.add
		}
		
		
		@mbox=Gtk::HBox.new(false, 0)
		@mbox.pack_start(@bexplo2m, true, false)
		@mbox.pack_start(@bm2explo, true, false)
		@mbox.pack_start(@bup, true, false)
		@mbox.pack_start(@bdown, true, false)
		@mbox.pack_start(@bplist2m, true, false)
		@mbox.pack_start(@bm2plist, true, false)
		
		#Radio
		@radiostore=Gtk::ListStore.new(String,String)
		@pref.radio.each{|key,value|
			child=@radiostore.append
			child[0]=key
			child[1]=value
			}
		
		@iadd_radio=Gtk::Image.new(Gtk::Stock::GO_FORWARD,Gtk::IconSize::MENU)
		@badd_radio=Gtk::ImageMenuItem.new(_('Add a radio'))
		@badd_radio.image=@iadd_radio
		@badd_radio=Gtk::ImageMenuItem.new(Gtk::Stock::GO_FORWARD)
		@irm_radio=Gtk::Image.new(Gtk::Stock::GO_BACK,Gtk::IconSize::MENU)
		@brm_radio=Gtk::ImageMenuItem.new(_('Delete a radio'))
		@brm_radio.image=@irm_radio
		@badd_radio.signal_connect("activate") {rm_radio}
		@bclear_radio=Gtk::ImageMenuItem.new(Gtk::Stock::CLEAR)
		@bclear_radio.signal_connect("activate") {
			@player.stop if @player.radio?
			@pref.radio.clear
			@radiostore.clear
		}
		@sep_radio=Gtk::SeparatorMenuItem.new
		@bedit_radio=Gtk::ImageMenuItem.new(Gtk::Stock::EDIT)#Gtk::MenuItem.new('Éditer')
		@bedit_radio.signal_connect("activate") {edit_radio(@radio_select.selected)}
		@popup_radio=Gtk::Menu.new
		@popup_radio.append(@badd_radio)
		@popup_radio.append(@brm_radio)
		@popup_radio.append(@bclear_radio)
		@popup_radio.append(@sep_radio)
		@popup_radio.append(@bedit_radio)
		@popup_radio.show_all
		
		@radioview=Gtk::TreeView.new(@radiostore)
		@radioview.headers_visible=false
		@radio_select=@radioview.selection
		@radio_select.mode = Gtk::SELECTION_SINGLE
		@renderer = Gtk::CellRendererText.new
		@col=Gtk::TreeViewColumn.new("Écouter la radio", @renderer, :text => 0)
		@radioview.append_column(@col)
		
		@radioview.signal_connect("row-activated"){@player.stop;play_radio}	
		@radioview.signal_connect("button_press_event"){|widget, event|
			if event.button == 3
				@popup_radio.popup(nil, nil, event.button, event.time)
			end
		}
		
		@wradio=Gtk::ScrolledWindow.new
		@wradio.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
		@wradio.add(@radioview)
		
		#Cd
		@cdstore=Gtk::ListStore.new(String)
		@cdview=Gtk::TreeView.new(@cdstore)
		@cdview.headers_visible=false
		@renderer = Gtk::CellRendererText.new
		@col=Gtk::TreeViewColumn.new("Lire un cd", @renderer, :text => 0)
		@cdview.append_column(@col)
		
		@wcd=Gtk::ScrolledWindow.new
		@wcd.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
		@wcd.add(@cdview)
		
		#Box
		@boxlist=Gtk::VBox.new(false, 0)
		@boxlist.pack_start(@combolist, false, false, 0)
		@boxlist.pack_start(@wlist1)
		@boxlist.pack_start(@wlist2)
		@boxlist.pack_start(@wradio)
		@boxlist.pack_start(@wcd)
		@boxlist.pack_start(@mbox,false, false, 0)

		###---Containers--------
		@table=Gtk::Table.new(5,2)
		@table.column_spacings=(5)
		@table.attach(@wtree, 0, 1, 0, 5)
		@table.attach(@badd, 1, 2, 1, 2, Gtk::SHRINK, Gtk::SHRINK)
		@table.attach(@bdel, 1, 2, 2, 3, Gtk::SHRINK, Gtk::SHRINK)
		@table.attach(@bclear, 1, 2, 3, 4, Gtk::SHRINK, Gtk::SHRINK)
		
		@paned=Gtk::HPaned.new
		#@paned.border_width=@border_width
		@paned.position=@pref.gui['paned_position']
		@paned.add1(@table)
		@paned.add2(@boxlist)
#**************Player buttons*********************
		#hide/show explo
		@iexplo=Gtk::Image.new('gtk-directory',Gtk::IconSize::BUTTON)#Gtk::Stock::DIRECTORY is not (yet?) initialized.
		@bexplo=Gtk::Button.new
		@bexplo.image=@iexplo
		@bexplo.signal_connect('clicked') {
			if @pref.gui['showexplo']
				@paned.child1.hide
				unless @pref.gui['showlist']
					@paned.hide;self.resize(400,100)
				end
			else
				if @pref.gui['showlist']
					s=self.size
		 			@pref.gui['width']=s[0];@pref.gui['height']=s[1]
		 		end
				@paned.child1.show;@paned.show;self.resize(@pref.gui['width'],@pref.gui['height'])
			end
			@pref.gui['showexplo']=!@pref.gui['showexplo']
			}
		#hide/show list
		@ilist=Gtk::Image.new(Gtk::Stock::INDEX,Gtk::IconSize::BUTTON)
		@blist=Gtk::Button.new
		@blist.image=@ilist
		@blist.signal_connect('clicked') {
			if @pref.gui['showlist']
				@paned.child2.hide
				unless @pref.gui['showexplo']
					@paned.hide;self.resize(400,100)
				end
			else
				if @pref.gui['showexplo']
					s=self.size
		 			@pref.gui['width']=s[0];@pref.gui['height']=s[1]
				end
				@paned.child2.show;@paned.show;self.resize(@pref.gui['width'],@pref.gui['height'])
			end
			@pref.gui['showlist']=!@pref.gui['showlist']
			}
		#Song name
		@stitle=Gtk::Label.new()
		@stitle.wrap=true
		
		@vtitle=Gtk::Frame.new#('Titre en cours')
		@vtitle.add(@stitle)
		#Volume
		@barvol=Gtk::VScale.new(0,100,1)
		@barvol.digits=0
		@barvol.inverted=true
		@barvol.value=@pref.player['volume']
		@barvol.signal_connect('value-changed'){@player.volume=@barvol.value/100}
		@barvol.show
		
		@wvol=Gtk::Window.new
		@wvol.decorated=false
		@wvol.skip_taskbar_hint=true
		@wvol.skip_pager_hint=true
		@wvol.set_size_request(40,200)
		@wvol.resizable=false
		#@wvol.transient_for=(self)
		@wvol.add(@barvol)
		@wvol.signal_connect("focus-out-event"){@wvol.hide}
		
		@ivol=Gtk::Image.new(File.join(Pdir,'volume.xpm'))
		@bvol=Gtk::Button.new
		@bvol.image=@ivol
		@barvol.can_focus=true
		@bvol.signal_connect('clicked') {
			position=self.position+@bvol.allocation.to_a
			@wvol.move(position[0]+position[2]-40,position[1]+position[3])
			@barvol.value=@player.volume*100
			@wvol.show
			}
		#Containers
		@boxh=Gtk::HBox.new(false)
		#@boxh.border_width=@border_width
		@boxh.pack_start(@bexplo, false, false)
		@boxh.pack_start(@blist, false, false)
		@boxh.pack_start(@vtitle, true, true)
		@boxh.pack_start(@bvol, false, false)
		
		#player
		@iprev=Gtk::Image.new(Gtk::Stock::MEDIA_PREVIOUS,Gtk::IconSize::BUTTON)
		@bprev= Gtk::Button.new
		@bprev.image=@iprev
		@bprev.signal_connect('clicked'){prev unless @player.radio?}
		@istop=Gtk::Image.new(Gtk::Stock::MEDIA_STOP,Gtk::IconSize::BUTTON)
		@bstop= Gtk::Button.new
		@bstop.image=@istop
		@bstop.signal_connect('clicked'){@player.stop}
		@iplay=Gtk::Image.new(Gtk::Stock::MEDIA_PLAY,Gtk::IconSize::BUTTON)
		@bplay= Gtk::Button.new
		@bplay.image=@iplay
		@bplay.signal_connect('clicked'){
			if @showlistn==3
				@player.stop
				play_radio
			else
				@player.stop if @player.radio?
				play
			end
			}
		@ipause=Gtk::Image.new(Gtk::Stock::MEDIA_PAUSE,Gtk::IconSize::BUTTON)
		@bpause=Gtk::Button.new
		@bpause.image=@ipause
		@bpause.signal_connect('clicked'){@player.pause}
		@inext=Gtk::Image.new(Gtk::Stock::MEDIA_NEXT,Gtk::IconSize::BUTTON)
		@bnext= Gtk::Button.new
		@bnext.image=@inext
		@bnext.signal_connect('clicked'){next_s unless @player.radio?}
		
		#song
		@lposition=Gtk::Label.new('--:--')
		@barpos=Gtk::HScale.new
		@barpos.draw_value=false
		@lduration=Gtk::Label.new('--:--')
		@barpos.signal_connect('button_press_event'){@seeking=true;false}
		@barpos.signal_connect('button_release_event'){@player.position=@barpos.value.to_i;@seeking=false}

		@bplayer = Gtk::HBox.new(false)
		#@bplayer.border_width=@border_width
		@bplayer.pack_start(@bprev, false, false)
		@bplayer.pack_start(@bstop, false, false)
		@bplayer.pack_start(@bplay, false, false)
		@bplayer.pack_start(@bpause, false, false)
		@bplayer.pack_start(@bnext, false, false)
		@bplayer.pack_start(@lposition, false, false)
		@bplayer.pack_start(@barpos, true, true)
		@bplayer.pack_start(@lduration, false, false)
#*************Quit************************
		@bclose= Gtk::Button.new(Gtk::Stock::QUIT)
		@bclose.signal_connect('clicked') {quit}
#*************Menu******************
		###----Musik menu-------
		@play_it=Gtk::ImageMenuItem.new(Gtk::Stock::MEDIA_PLAY)
		@play_it.signal_connect('activate'){@player.play}
		@stop_it=Gtk::ImageMenuItem.new(Gtk::Stock::MEDIA_STOP)
		@stop_it.signal_connect('activate'){@player.stop}
		@prev_it=Gtk::ImageMenuItem.new(Gtk::Stock::MEDIA_PREVIOUS)
		@prev_it.signal_connect('activate'){prev}
		@next_it=Gtk::ImageMenuItem.new(Gtk::Stock::MEDIA_NEXT)
		@next_it.signal_connect('activate'){next_s}
		@sep=Gtk::SeparatorMenuItem.new
		@sep2=Gtk::SeparatorMenuItem.new
		@shuffle_it=Gtk::CheckMenuItem.new(_('Shuffle'));
		@shuffle_it.active=@pref.player['shuffle']
		@shuffle_it.signal_connect("toggled"){@pref.player['shuffle']=!@pref.player['shuffle'];@player.toggle_shuffle}
		@repeat_it=Gtk::CheckMenuItem.new(_('Repeat'));#repeat or loopback or????
		@repeat_it.active=@pref.player['repeat']
		@repeat_it.signal_connect("toggled"){
			@pref.player['repeat']=!@pref.player['repeat'];@player.repeat=@pref.player['repeat']
		}
		@playonstart_it=Gtk::CheckMenuItem.new(_('Play on start'))
		@playonstart_it.active=@pref.player['playonstart']
		@playonstart_it.signal_connect("toggled"){@pref.player['playonstart']=!@pref.player['playonstart']}
		@quit_it=Gtk::ImageMenuItem.new(Gtk::Stock::QUIT)
		@quit_it.signal_connect('activate'){quit}
		
		@menu_musiq=Gtk::Menu.new
		@menu_musiq.append(@play_it)
		@menu_musiq.append(@stop_it)
		@menu_musiq.append(@prev_it)
		@menu_musiq.append(@next_it)
		@menu_musiq.append(@sep)
		@menu_musiq.append(@shuffle_it)
		@menu_musiq.append(@repeat_it)
		@menu_musiq.append(@playonstart_it)
		@menu_musiq.append(@sep2)
		@menu_musiq.append(@quit_it)
		###----Explorateur menu------
		@tree_refresh_it=Gtk::ImageMenuItem.new(Gtk::Stock::REFRESH)
		@tree_refresh_it.signal_connect("activate") {@path_explo=@treestore.refresh(@pref.dir,@pref.ext)}
		@moddir_it=Gtk::ImageMenuItem.new(Gtk::Stock::PREFERENCES)
		@moddir_it.signal_connect("activate") {
			dialog=Option.new(self,@pref.dir,@pref.ext,@border_width)
			if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
				@pref.dir=dialog.dir
				@pref.ext=dialog.ext
				@path_explo=@treestore.refresh(@pref.dir,@pref.ext)
			end  
			dialog.destroy
		}
		@search_it=Gtk::ImageMenuItem.new(Gtk::Stock::FIND)
		@search_it.signal_connect("activate") {
			@wsearch_explo=Search::Explo.new(@player,@list1,@list2,@border_width) unless Search.explo?
			position=self.position+@wtree.allocation.to_a
			@wsearch_explo.search(position,@path_explo)
		}
		
		@menu_explo=Gtk::Menu.new
		@menu_explo.append(@tree_refresh_it)
		@menu_explo.append(@moddir_it)
		@menu_explo.append(@search_it)
		###----Playlist menu------
		@list_refresh_it=Gtk::ImageMenuItem.new(Gtk::Stock::REFRESH)
		@list_refresh_it.signal_connect("activate") {@list1.refresh}
		@import_it=Gtk::ImageMenuItem.new(Gtk::Stock::ADD)
		@import_it.signal_connect("activate") {@list1.import(self);@player.add}
		@export_it=Gtk::ImageMenuItem.new(Gtk::Stock::SAVE)
		@export_it.signal_connect("activate") {@list1.export(self)}
		@del_it=Gtk::ImageMenuItem.new(Gtk::Stock::CLEAR)
		@del_it.signal_connect("activate") {@player.stop;@player.del_all;@list1.del_all}
		@search_plit=Gtk::ImageMenuItem.new(Gtk::Stock::FIND)
		@search_plit.signal_connect("activate"){
			@combolist.active=0
			@wsearch_plist=Search::List.new unless Search.plist?
			position=self.position+@boxlist.allocation.to_a
			@wsearch_plist.search(position,@list1,@listview1)
		}
		@sep=Gtk::SeparatorMenuItem.new
		@list_edit_it=Gtk::ImageMenuItem.new(Gtk::Stock::EDIT)
		@list_edit_it.signal_connect("activate") {
			@list1_select.select_all
			EditTag.new(self,@list1_select,@list1,@border_width)
		}
		
		@menu_list=Gtk::Menu.new
		@menu_list.append(@list_refresh_it)
		@menu_list.append(@import_it)
		@menu_list.append(@export_it)
		@menu_list.append(@del_it)
		@menu_list.append(@search_plit)
		@menu_list.append(@sep)
		@menu_list.append(@list_edit_it)
		###----Help menu
#Faire un fichier help_lc.rb par langue
#Utiliser GetText.locale afin de connaitre lc
		@help_it2=Gtk::ImageMenuItem.new(Gtk::Stock::HELP)
		@help_it2.signal_connect("activate"){Help.new(self)}
		@about_it=Gtk::ImageMenuItem.new(Gtk::Stock::ABOUT)
		f=File.open(File.join(Ddir,'COPYING'))
		text=''
		while line=f.gets
			text+=line
		end
		f.close	
		@about_it.signal_connect("activate") {
		Gtk::AboutDialog.show(self, {'name'=>'ZiK',
		'version'=>Version,
		'copyright'=>'Copyright 2007-2008 Vincent Carmona',
		'comments'=>_('Audio player'),
		'website'=>'http://zik.rubyforge.org/',
		'license'=>text,
		'authors'=>["Vincent Carmona <vinc-mai@caramail.com>"],
		'translator_credits'=>"Français: Vincent Carmona\n",
		'logo'=>Gdk::Pixbuf.new(File.join(Pdir,'ZiK3.png'))})
#name do not work -> Bug 496689?
		}
		
		@menu_help=Gtk::Menu.new
		@menu_help.append(@help_it2)
		@menu_help.append(@about_it)
		###----Menu bar------------
		@musiq_it=Gtk::MenuItem.new(_('Music'))
		@musiq_it.set_submenu(@menu_musiq)
		@list_it=Gtk::MenuItem.new(_('Playlist'))
		@list_it.set_submenu(@menu_list)
		@config_it=Gtk::MenuItem.new(_('Browser'))
		@config_it.set_submenu(@menu_explo)
		@help_it=Gtk::MenuItem.new(_('Help'))
		@help_it.set_submenu(@menu_help)
		
		@menu_bar=Gtk::MenuBar.new
		@menu_bar.append(@musiq_it)
		@menu_bar.append(@config_it)
		@menu_bar.append(@list_it)
		@menu_bar.append(@help_it)
#****************Acceleration Keys*********************
#connect media key 0xa2(play/pause)
		@accel=Gtk::AccelGroup.new
		@accel.connect(Gdk::Keyval::GDK_p, nil,Gtk::ACCEL_VISIBLE){
			if @player.playing?
				@player.pause
			else
				if @player.radio?
					play_radio
				else
					play
				end
			end
			}
		@accel.connect(Gdk::Keyval::GDK_s, nil,Gtk::ACCEL_VISIBLE){@player.stop}
		@accel.connect(Gdk::Keyval::GDK_b, nil,Gtk::ACCEL_VISIBLE){prev}
		@accel.connect(Gdk::Keyval::GDK_n, nil,Gtk::ACCEL_VISIBLE){next_s}
		@accel.connect(Gdk::Keyval::GDK_Down, Gdk::Window::CONTROL_MASK,Gtk::ACCEL_VISIBLE){@player.vol_down}
		@accel.connect(Gdk::Keyval::GDK_Up, Gdk::Window::CONTROL_MASK,Gtk::ACCEL_VISIBLE){@player.vol_up}
		@accel.connect(Gdk::Keyval::GDK_f, Gdk::Window::CONTROL_MASK, Gtk::ACCEL_VISIBLE){
			@combolist.active=0
			@wsearch_plist=Search::List.new unless Search.plist?
			position=self.position+@boxlist.allocation.to_a
			@wsearch_plist.search(position,@list1,@listview1)
		}
#***************Main window********************
		###---Container---
		@vbox=Gtk::VBox.new
		@vbox.pack_start( @menu_bar, false, false, 0)
		@vbox.pack_start(@paned)
		@vbox.pack_start(@boxh, false, false)
		@vbox.pack_start(@bplayer, false, false)
		@vbox.pack_start(@bclose, false, false)
		#Pre show the widgets
		@vbox.show_all
		@wlist2.hide;@mbox.hide;@wradio.hide;@wcd.hide
		if !@pref.gui['showexplo']
			@paned.child1.hide
		end
		if !@pref.gui['showlist']
			@paned.child2.hide
		end
		###---Window-----
		super(Gtk::Window::TOPLEVEL)
		self.signal_connect("grab-focus"){|widget, event| puts 'grab';@wvol.hide}
		self.signal_connect("delete_event") {quit;true}
		self.add_accel_group(@accel)
		self.set_default_size(500,600)
		if  @pref.gui['showlist'] or @pref.gui['explo']
			self.resize(@pref.gui['width'],@pref.gui['height'])
		else
			self.resize(400,100)
		end
		self.move(@pref.gui['xposition'],@pref.gui['yposition'])
		self.title='ZiK'
		@icon_Z = Gdk::Pixbuf.new(File.join(Pdir,'ZiK.svg'))
		self.icon=(@icon_Z)
		self.add(@vbox)
		
		if @pref.gui['tray']
			@tray=SysTray.new(self,@player)
		end
		play if @pref.player['playonstart']
	end


#************Refresh the datas when playing****************************
	def play(song=nil)
		@player.play(song)
		@pref.player['current']=@player.current
		current_shuffle=@player.order[@player.current]
		unless current_shuffle.nil?
			name=@list1.name[current_shuffle]
			duration=(@list1.duration[current_shuffle].to_i)
			@stitle.set_markup("<b>#{name}</b>")
			if duration <= 0
				show_duration=false
				@lduration.text='--:--'
			else
				show_duration=true
				@barpos.set_range(0,duration)
				duration_s=(Time.at(duration).strftime("%M:%S"))
				@lduration.text=duration_s
			end
			@list1.store.each{|model,path,iter|
				if iter[0]==name
					iter[1]=true
				else
					iter[1]=false
				end
			}
		end
		
			Gtk.timeout_add(400){
				unless @pref.player['current']==@player.current
					@pref.player['current']=@player.current
					current_shuffle=@player.order[@player.current]
					unless current_shuffle.nil?
						name=@list1.name[current_shuffle]
						@stitle.set_markup("<b>#{name}</b>")
						duration=(@list1.duration[current_shuffle].to_i)
						if duration <= 0
							show_duration=false
							@lduration.text='--:--'
						else
							show_duration=true
							@barpos.set_range(0,duration)
							duration_s=(Time.at(duration).strftime("%M:%S"))
							@lduration.text=duration_s
						end
						@list1.store.each{|model,path,iter|#In the playlist view, set the current song to bold
							if iter[0]==name
								iter[1]=true
							else
								iter[1]=false
							end
						}
					end
				end
				position=@player.position
				unless position.nil?
					unless @seeking
					@barpos.value=position
					end
					if position >= 0
					@lposition.text=Time.at(position).strftime("%M:%S")
					else
						@lposition.text='--:--'
					end
				end
				@player.playing?
				}
	end
	
	def play_radio #Gérer les erreurs du type "Gst error" -> utiliser array[1..end] 
		unless @radio_select.selected.nil?
			url=@radio_select.selected[1]
			case url
			when /^http:\/\/.*\.m3u$/i
				plist=fetch_playlist(url)
				tmp=Tempfile.new('ZiKPlist')
				tmp.puts plist
				tmp.close; tmp.open
				array=read_m3u(tmp)
				tmp.close!
				stream=array
			when /^http:\/\/.*\.pls$/i
				plist=fetch_playlist(url)
				tmp=Tempfile.new('ZiKPlist')
				tmp.puts plist
				tmp.close; tmp.open
				array=read_pls(tmp)
				tmp.close!
				stream=array
			when /^http:\/\/.*\.asx$/i
				plist=fetch_playlist(url)
				tmp=Tempfile.new('ZiKPlist')
				tmp.puts plist
				tmp.close; tmp.open
				array=read_asx(tmp)
				tmp.close!
				stream=array
			else
				stream=[url]
			end
			radio_name=@radio_select.selected[0]
			@stitle.set_markup("<b>#{radio_name}</b>")
			@lduration.text='--:--'
			@barpos.value=0
			i=0;l=stream.length
			begin
				@player.play_radio(stream[i])
			rescue GstError
				i+=1
				@player.play_radio(stream[i]) if i < l
			end
		end
	end
	
	def next_s
		play unless @player.playing?
		@player.next_s
	end
	
	def prev
		play unless @player.playing?
		@player.prev
	end
	
	def add_radio
		entry=Gtk::Entry.new
		entry.text=_('Name')
		entry2=Gtk::Entry.new
		entry2.text=_('Url')
		dialog = Gtk::Dialog.new(_("Add a radio"),self,Gtk::Dialog::DESTROY_WITH_PARENT,[Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL], [Gtk::Stock::APPLY, Gtk::Dialog::RESPONSE_ACCEPT])
		dialog.vbox.add(entry)
		dialog.vbox.add(entry2)
		dialog.show_all
		if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
			@pref.radio[entry.text]=entry2.text
			child=@radiostore.append
			child[0]=entry.text;child[1]=entry2.text
		end
		dialog.destroy
	end
	
	def rm_radio
		unless @radio_select.selected.nil?
			@pref.radio.delete_at(@radio_select.selected.path.to_str.to_i)
			@radiostore.remove(@radio_select.selected)
		end
	end
	
	def edit_radio(selection)
		unless selection.nil?
			entry=Gtk::Entry.new
			entry.text=selection[0]
			entry2=Gtk::Entry.new
			entry2.text=selection[1]
			dialog = Gtk::Dialog.new(_("Edit a radio"),self,Gtk::Dialog::DESTROY_WITH_PARENT,[Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL], [Gtk::Stock::APPLY, Gtk::Dialog::RESPONSE_ACCEPT])
			dialog.vbox.add(entry)
			dialog.vbox.add(entry2)
			dialog.show_all
			if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
				@pref.radio.delete(selection[0])
				@pref.radio[entry.text]=entry2.text
				selection[0]=entry.text;selection[1]=entry2.text
			end
			dialog.destroy
		end
	end
	
	def quit
		 @player.stop
		 @list1.write
		 @pref.player['volume']=(@player.volume*100)
		 pos=self.position
		 @pref.gui['xposition']=pos[0]
		 @pref.gui['yposition']=pos[1]
		 if  @pref.gui['showlist'] or @pref.gui['explo']
		 	s=self.size
		 	@pref.gui['width']=s[0]
		 	@pref.gui['height']=s[1]
		 end
		 @pref.gui['paned_position']=@paned.position
		 @pref.write
		 Gtk.main_quit
		 puts "Bye!"
	end
end
