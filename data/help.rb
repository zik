=begin
Copyright 2007-2008 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

class Help < Gtk::Window
	def initialize(wmain,border=10)
		@izik=Gdk::Pixbuf.new(File.join(Pdir,'ZiK4.png'))
		
		@textview=Gtk::TextView.new
		@textview.editable=false
		@textview.wrap_mode=Gtk::TextTag::WRAP_WORD
		@buffer=@textview.buffer
		
		@buffer.create_tag('heading','weight' => Pango::FontDescription::WEIGHT_BOLD,'size' => 15 * Pango::SCALE)
		@buffer.create_tag('menu','background'=>'grey')
		
		@iter=@buffer.get_iter_at_offset(0)
		@buffer.insert(@iter,"Présentation\n",'heading')
		@buffer.insert(@iter,@izik)
		@buffer.insert(@iter,"est un lecteur audio.\nÀ la différence d'autres lecteurs, il ne présente pas de bibliothèque audio. Il s'appuye sur l'arborescence de vos répertoires pour afficher un \"explorateur de fichiers audio\".\n")
		
		@buffer.insert(@iter,"\nPremiers pas\n",'heading')
		@buffer.insert(@iter,"Dans un premier temps, il faut indiquer à l'explorateur les dossiers dont vous souhaitez afficher le contenu audio ainsi les types de fichiers qui seront listés.")
		@buffer.insert(@iter," Ce réglage s'effectue dans le menu ")
		@buffer.insert(@iter,'explorateur, préfèrences','menu')
		@buffer.insert(@iter,". L'explorateur affiche automatiquement les sous-dossiers qui contiennent des fichiers audios. Seuls les fichiers dont l \'extension est déclarée sont affichés.")
		@buffer.insert(@iter,"\nL'ajout de fichiers audio depuis l'explorateur est permise grâce au bouton")
		@add_anchor=@buffer.create_child_anchor(@iter)
		@buffer.insert(@iter,". La liste de lecture peut être complétée à l'aide du menu ")
		@buffer.insert(@iter,"Liste de lecture, ajouter",'menu');@buffer.insert(@iter,".\n")
		@buffer.insert(@iter,"Les boutons ")
		@hide_anchor1=@buffer.create_child_anchor(@iter)
		@hide_anchor2=@buffer.create_child_anchor(@iter)
		@buffer.insert(@iter," permettent de cacher et d'afficher l'explorateur et la liste de lecture.\n")
		
		@buffer.insert(@iter,"\nModifier une liste\n",'heading')
		@buffer.insert(@iter,"Choisissez ")
		@combo_anchor=@buffer.create_child_anchor(@iter)
		@buffer.insert(@iter," afin de modifier une liste. Plusieurs possibilités sont disponibles: ")
		@import_anchor=@buffer.create_child_anchor(@iter)
		@buffer.insert(@iter," importer une liste, ")
		@export_anchor=@buffer.create_child_anchor(@iter)
		@buffer.insert(@iter," exporter la liste, ")
		@up_anchor=@buffer.create_child_anchor(@iter)
		@down_anchor=@buffer.create_child_anchor(@iter)
		@buffer.insert(@iter," déplacer des chansons, ")
		@copy_anchor=@buffer.create_child_anchor(@iter)
		@buffer.insert(@iter," ajouter à la liste la liste de lecture et ")
		@paste_anchor=@buffer.create_child_anchor(@iter)
		@buffer.insert(@iter,"ajouter la liste à la liste de lecture actuellement jouée par le lecteur.\n")
		
		@buffer.insert(@iter,"\nÉdition de tag\n",'heading')
		@buffer.insert(@iter,"Pour éditer les tag d\'un fichier audio, le sélectionner dans la liste de lecture puis choisir éditer à l'aide d'un clic droit.")
		@buffer.insert(@iter,"Le bouton enregistrer permet de sauvegarder définitivement le tag actif. Le bouton cancel sort de l\'éditeur sans modifier les tags non sauvegarder. Le bouton ok enregistre tous les tags puis ferme l'\éditeur.")
		
		@buffer.insert(@iter,"\nÉcouter la radio\n",'heading')
		@buffer.insert(@iter,"Sélectionner ")
		@combo2_anchor=@buffer.create_child_anchor(@iter)
		@buffer.insert(@iter,". Pour ajouter l'url d'une radio, utiliser le bouton ")
		@add2_anchor=@buffer.create_child_anchor(@iter)
		@buffer.insert(@iter,". Un double-clic sur la radio de votre choix permet de l'écouter.\n")
		
		@buffer.insert(@iter,"\nFormats supportés\n",'heading')
		@buffer.insert(@iter,"fichiers: wav, flac, ogg, mp3, wma.\n")
		@buffer.insert(@iter,"listes: m3u,pls.\n")
		@buffer.insert(@iter,"radios: http and mms protocol.\n")
		
		@buffer.insert(@iter,"\nRaccourci clavier\n",'heading')
		@buffer.insert(@iter,"Lecture/pause: p,\nsuivant: n,\nprécédant: b,\naugmenter/diminuer le volume: Ctrl + flèche du haut/bas.\n")
		
		@badd=Gtk::Button.new.add(Gtk::Image.new(Gtk::Stock::GO_FORWARD,Gtk::IconSize::BUTTON))
		@textview.add_child_at_anchor(@badd,@add_anchor)
		@bhide1=Gtk::Button.new.add(Gtk::Image.new('gtk-directory',Gtk::IconSize::BUTTON))
		@bhide2=Gtk::Button.new.add(Gtk::Image.new(Gtk::Stock::INDEX,Gtk::IconSize::BUTTON))
		@textview.add_child_at_anchor(@bhide1,@hide_anchor1)
		@textview.add_child_at_anchor(@bhide2,@hide_anchor2)
		@combo=Gtk::ComboBox.new
		@combo.append_text('Liste de lecture')
		@combo.append_text('Gérer une liste secondaire')
		@combo.append_text('Écouter la radio')
		@combo.active=1
		@textview.add_child_at_anchor(@combo,@combo_anchor)
		@bimport=Gtk::Button.new.add(Gtk::Image.new(Gtk::Stock::ADD,Gtk::IconSize::BUTTON))
		@textview.add_child_at_anchor(@bimport,@import_anchor)
		@bexport=Gtk::Button.new.add(Gtk::Image.new(Gtk::Stock::SAVE,Gtk::IconSize::BUTTON))
		@textview.add_child_at_anchor(@bexport,@export_anchor)
		@bup=Gtk::Button.new.add(Gtk::Image.new(Gtk::Stock::GO_UP,Gtk::IconSize::BUTTON))
		@textview.add_child_at_anchor(@bup,@up_anchor)
		@bdown=Gtk::Button.new.add(Gtk::Image.new(Gtk::Stock::GO_DOWN,Gtk::IconSize::BUTTON))
		@textview.add_child_at_anchor(@bdown,@down_anchor)
		@bcopy=Gtk::Button.new.add(Gtk::Image.new(Gtk::Stock::COPY,Gtk::IconSize::BUTTON))
		@textview.add_child_at_anchor(@bcopy,@copy_anchor)
		@bpaste=Gtk::Button.new.add(Gtk::Image.new(Gtk::Stock::PASTE,Gtk::IconSize::BUTTON))
		@textview.add_child_at_anchor(@bpaste,@paste_anchor)
		@combo2=Gtk::ComboBox.new
		@combo2.append_text('Liste de lecture');@combo2.append_text('Gérer une liste secondaire');@combo2.append_text('Écouter la radio')
		@combo2.active=2
		@textview.add_child_at_anchor(@combo2,@combo2_anchor)
		@badd2=Gtk::Button.new.add(Gtk::Image.new(Gtk::Stock::GO_FORWARD,Gtk::IconSize::BUTTON))
		@textview.add_child_at_anchor(@badd2,@add2_anchor)
		
		@scroll=Gtk::ScrolledWindow.new
		@scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
		@scroll.add(@textview)
		super(Gtk::Window::TOPLEVEL)
		#self.transient_for=wmain
		self.destroy_with_parent=true
		self.title='Aide'
		self.set_default_size(600,600)
		self.border_width=border
		self.add(@scroll)
		
		self.show_all
	end
end
