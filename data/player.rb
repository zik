=begin
Copyright 2007-2008 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end
class GstError < Exception
end

class Player
	attr_accessor :current
	attr_writer :repeat
	attr_reader :order
	def initialize(list,preference)
		@playlist=list.path
		@current=preference['current']
		@playing=false
		@radio=false
		@shuffle=preference['shuffle']
		if @shuffle
			@current=0
		end
		@repeat=preference['repeat']
		@order=[]
		for i in 0..@playlist.length-1
			@order.push(i)
		end
		if @shuffle
			@order=@order.sort_by{rand}
		end
		
		@playbin=Gst::ElementFactory.make('playbin')
		@playbin.ready
		
		if !@playlist[@current].nil?
			@playbin.uri='file://'+@playlist[@current]
		end
		@playbin.bus.add_watch { | message | got_bus_message(message) }
	end
	
	def playing?
		@playing
	end
	
	def radio?
		@radio
	end
	
	def got_bus_message(message)
		case message.get_type
			when Gst::Message::MessageType::ERROR
				$stderr.puts 'Gst error'
				@playbin.stop
				raise GstError.new if @radio
			when Gst::Message::MessageType::EOS
				self.next_s
		end
		true #Without this line, only the first message is processed
	end
	
	def add
		played=@order[0..@current]
		length=@order.length
		not_played=@order[@current+1..length-1]
		if not_played.nil?
			not_played=[]
		end
		for i in length..@playlist.length-1
			not_played.push(i)
		end
		if @shuffle
			not_played=not_played.sort_by{rand}			
		end
		@order=played+not_played
	end
	
	def del(select)
		select.each{|sel|
			i=0
			@order.each{|n|
				case (n <=> sel)
					when 0 then
						@order[i]=nil
					when 1 then
						@order[i]-=1
				end
				i+=1
				}
			@order.compact!
			case sel <=> @order[@current]
				when 0 then
					stop
					play
				when -1 then
					@current-=1
				when nil then#@order[@current] returns nil if playing the last song.
					@current-=1
					next_s if sel==@order[@current]+1#if deleting the last song
			end
			}
	end
	
	def del_all
		stop
		@order.clear
		@current=0
	end
	
	def toggle_shuffle#Jouer ou pas les chansons déjà jouées?
		@shuffle=!@shuffle
		if @shuffle
			@order=@order.sort_by{rand}
			temp=@order.index(@current)
			@current=temp
			@current=0 if @current.nil?
		else
			@current=@order[@current]
			@current=0 if @current.nil?
			@order.sort!
		end
	end

	def play(song=nil)
		begin
			@playing=true
			@radio=false
			if song.nil?
				n=@order[@current]
			else
				n=song
				@current=@order.index(n)
			end
			name=@playlist[n]
			@playbin.uri='file://'+name
			@playbin.play
			puts "Playing... #{name}"
		rescue
			$stderr.puts "Error playing file://#{name}"
		end
	end
	
	def play_radio(radio)
		begin
			@radio=true;@playing=true
			@playbin.stop
			@playbin.uri=radio
			@playbin.play
			puts "Playing... #{radio}"
			#puts @playbin.stream_info
		rescue
			$stderr.puts "Cannot play #{radio}"
		end
	end
	
	def pause
		@playbin.pause
		puts 'Pause.'
		@playing=false
	end
	
	def stop
		@playbin.stop
		@playing=false
	end
	
	def next_s
		if @current < @playlist.length-1
			@current+=1
			stop
			play
		else
			if @repeat
				@playbin.stop
				@current=0
				play
			else
				puts 'End of playlist.'
				stop
			end
		end
	end
	
	def prev
		if @current > 0
			@current-=1
			@playbin.stop
			play
		else
			if @repeat
				@playbin.stop
				@current=@order.length-1
				play
			else
			puts 'Begin of playlist.'
			end
		end
	end
	
	def volume
		@playbin.volume
	end
	def volume=(n)
		@playbin.volume=n
	end
	def vol_up
		if @playbin.volume>0.9
			@playbin.volume=1
		else
			@playbin.volume+=0.1
		end
	end
	def vol_down
		if @playbin.volume<0.1
			@playbin.volume=0
		else
			@playbin.volume-=0.1
		end
	end
	
	def position
 		@playbin.query_position[1]/1000000000
	end
	
	def position=(pos)
		@playbin.seek(1.0, Gst::Format::Type::TIME,Gst::Seek::FLAG_FLUSH.to_i | Gst::Seek::FLAG_KEY_UNIT.to_i,Gst::Seek::TYPE_SET,
		pos*1000000000,Gst::Seek::TYPE_NONE,-1)#Code from the Gst binding examples (cf:video-player)
	end
end
