=begin
Copyright 2007-2008 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end
include GetText
bindtextdomain('ZiK',{:path=>Ldir})
class EditTag	< Gtk::Window
	@@nb_instance=0
	def initialize(window,selection,list,border=10)
		if @@nb_instance==0
			@selection=selection; @tree_path=[]
			@path=list.path; @num=[]; @name=list.name
			@tag=[]; @title=[]; @artist=[]
			@xalign=0.1
			
			@notebook=Gtk::Notebook.new
			@notebook.scrollable=true
			@notebook.enable_popup=true
			@selection.selected_each{|model,path,iter|
				title_entry=Gtk::Entry.new; @title.push(title_entry)
				title_frame=Gtk::Frame.new(_('Title'))
				title_frame.label_xalign=@xalign
				title_frame.add(title_entry)
				artist_entry=Gtk::Entry.new; @artist.push(artist_entry)
				artist_frame=Gtk::Frame.new(_('Artist'))
				artist_frame.label_xalign=@xalign
				artist_frame.add(artist_entry)
				info=Gtk::Label.new
				info_frame=Gtk::Frame.new(_('Informations'))
				info_frame.label_xalign=@xalign
				info_frame.add(info)
				
				vbox=Gtk::VBox.new(false)
				vbox.pack_start(title_frame)
				vbox.pack_start(artist_frame)
				vbox.pack_start(info_frame)		
				name=Gtk::Label.new(iter[0])
				@notebook.append_page_menu(vbox,name)
				
				n=path.to_str.to_i
				begin
					tag=TagLib::File.new(@path[n]); @tag.push(tag)
					title_entry.text=tag.title
					artist_entry.text=tag.artist
					length=tag.length
					bitrate=tag.bitrate
					samplerate=tag.samplerate
					channels=tag.channels
					info.text=_("Durée: #{length}\ndébit: #{bitrate}\néchantillonage: #{samplerate}\ncanaux: #{channels}")
				rescue TagLib::BadTag
					vbox.pack_start(Gtk::Label.new.set_markup(_("<b><span foreground='red'>Error: Cannot read tag.</span></b>")))
				rescue	TagLib::BadAudioProperties
					info="Non accessible."
				end
				@num.push(n)
				@tree_path.push(path)
				
			}
			
			@bsave=Gtk::Button.new(Gtk::Stock::SAVE)
			@bsave.signal_connect('clicked'){save(@notebook.page)}
			@bcancel=Gtk::Button.new(Gtk::Stock::CANCEL)
			@bcancel.signal_connect('clicked'){self.destroy}
			@bok=Gtk::Button.new(Gtk::Stock::OK)
			@bok.signal_connect('clicked'){ok}
			@close_box=Gtk::HBox.new(false)
			@close_box.pack_start(@bsave,false)
			@close_box.pack_start(Gtk::Label.new)
			@close_box.pack_start(@bcancel,false)
			@close_box.pack_start(@bok,false)
			
			@vbox=Gtk::VBox.new(false)
			@vbox.pack_start(@notebook)
			@vbox.pack_start(@close_box, false)
			super(Gtk::Window::TOPLEVEL)
			self.signal_connect("destroy") {quit}
			self.title=_('Edit tags')
			self.icon=Gdk::Pixbuf.new(File.join(Pdir,'ZiK.svg'))
			self.add(@vbox)
			self.show_all
			@@nb_instance+=1
		else
			puts "Tag\'s editor is already opened"
		end
	end
	
	def save(num)
		begin
			title=@title[num].text
			artist=@artist[num].text
			name=artist+' - '+title
			if name==' - '
				p=@path[@num[num]]
				ext=File.extname(p)
				name=File.basename(p,ext)
			end
			@tag[num].title=title
			@tag[num].artist=artist
			@tag[num].save
			@name[@num[num]]=name#Change the name in the playlist object
			@selection.tree_view.model.get_iter(@tree_path[num])[0]=name#Change the name in the treeview
			puts "#{@path[@num[num]]} file\'s tag saved."
		rescue
			message=
			$stderr.puts "Error saving #{@path[@num[num]]} file\'s tag"
			dialog=Gtk::MessageDialog.new(self, Gtk::Dialog::MODAL, Gtk::MessageDialog::ERROR,
				Gtk::MessageDialog::BUTTONS_CLOSE, _("Error saving #{@path[@num[num]]} file\'s tag"))
			dialog.run
			dialog.destroy
		end
	end
	
	def quit
		@tag.each{|tag| tag.close}
		@@nb_instance-=1
		puts "Tag's editor closed."
	end
	
	def ok
		for i in 0..@num.length-1
			save(i)
		end
		self.destroy
	end
end
