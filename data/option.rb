=begin
Copyright 2007 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end
include GetText
bindtextdomain('ZiK',{:path=>Ldir})
class Option < Gtk::Dialog
	attr_reader :dir,:ext
	def initialize(window,dir,pref_ext,border=10)
#***********Directories to display**********************
		@dir=dir
		
		@brm=Gtk::Button.new(Gtk::Stock::REMOVE)
		@brm.signal_connect('clicked') {del}	
		@badd=Gtk::Button.new(Gtk::Stock::ADD)
		@badd.signal_connect('clicked') {add}
		@bclear=Gtk::Button.new(Gtk::Stock::CLEAR)
		@bclear.signal_connect('clicked') {clear}
		@bbox=Gtk::HBox.new(true, 0)
		@bbox.pack_start(@badd, false, false)
		@bbox.pack_start(@brm, false, false)
		@bbox.pack_start(@bclear, false, false)
		
		@store=Gtk::ListStore.new(String)
		@dir.each{|name|
			child=@store.append
			child[0]=name
		}
		@listview=Gtk::TreeView.new(@store)
		@listview.headers_visible=false
		@list_select=@listview.selection
		@list_select.mode = Gtk::SELECTION_MULTIPLE
		@renderer = Gtk::CellRendererText.new
		@col=Gtk::TreeViewColumn.new("Dossiers racine", @renderer, :text => 0)
		@listview.append_column(@col)
		
		@dirbox=Gtk::VBox.new
		@dirbox.border_width=border
		@dirbox.pack_start(@bbox, false, false)
		@dirbox.pack_start(@listview, false, false)
		
#****************Format to display************************
		@ext=pref_ext
		@button={}
		@ext.each{|ext,show|
			@button[ext]=Gtk::CheckButton.new(ext)
			@button[ext].active=show
		}
		@button.each{|ext,button|
			button.signal_connect('toggled'){@ext[ext]=button.active?}
		}
		
		@table=Gtk::Table.new(4,2)
		@table.border_width=border
		@table.attach(@button['wav'], 0, 1, 0, 1)
		@table.attach(@button['flac'], 0, 1, 1, 2)
		@table.attach(@button['ogg'], 0, 1, 2, 3)
		@table.attach(@button['mp3'], 1, 2, 0, 1)
		@table.attach(@button['wma'], 1, 2, 1, 2)
		@table.attach(@button['m3u'], 1, 2, 2, 3)
		@table.attach(@button['pls'], 1, 2, 3, 4)
#****************************************		
		
		@book=Gtk::Notebook.new
		@book.append_page(@dirbox,Gtk::Label.new(_('Directories to show')))
		@book.append_page(@table,Gtk::Label.new(_('Formats to list')))
		
		super(_('Preferences'),window,Gtk::Dialog::MODAL,[Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_REJECT],[Gtk::Stock::OK, Gtk::Dialog::RESPONSE_ACCEPT])
		self.icon=Gdk::Pixbuf.new(File.join(Pdir,'ZiK.svg'))
		self.vbox.pack_start(@book)
		self.show_all
	end
	
	def add
		dialog=Gtk::FileChooserDialog.new(_("Add a directory"), self,
			Gtk::FileChooser::ACTION_SELECT_FOLDER, nil,
			[Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL], [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
		if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
			n=dialog.filename
			@dir.push(n)
			child=@store.append
			child[0]=n
		end
		dialog.destroy
	end
	
	def del
		itera=[];patha=[]
		@list_select.selected_each{|model, path, iter|
			itera.push(iter)
			patha.push(path.to_s.to_i)
		}
		itera.each{|i| @store.remove(i)}
		patha.sort!; n=0
		patha.each{|a| @dir.delete_at(a-n);n+=1}
	end
	
	def clear
		@store.clear
		@dir.clear
	end

end
