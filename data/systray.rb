=begin
Copyright 2007-2008 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end
include GetText
bindtextdomain('ZiK',{:path=>Ldir})

class SysTray < Gtk::StatusIcon	
	def initialize(window,player)
		@player=player
		
		@bplay_tray=Gtk::CheckMenuItem.new(_('Play'))
		@bplay_tray.signal_connect("toggled"){
			if @show_tray #Do not process when setting the toggled state on showing the  popup menu.
				if  @player.playing?
					@player.pause				
				else
					if @showlistn==3
						@player.stop
						window.play_radio
					else
						window.play
					end
				end
			end
		}
		@bprev_tray=Gtk::ImageMenuItem.new(Gtk::Stock::MEDIA_PREVIOUS)
		@bprev_tray.signal_connect("activate"){window.prev}
		@bnext_tray=Gtk::ImageMenuItem.new(Gtk::Stock::MEDIA_NEXT)
		@bnext_tray.signal_connect("activate"){window.next_s}
		@bquit_tray=Gtk::ImageMenuItem.new(Gtk::Stock::QUIT)
		@bshow_tray=Gtk::CheckMenuItem.new(_('Show player'))
		@bshow_tray.signal_connect("toggled"){
			if @show_tray #Do not process when setting the toggled state on showing the  popup menu.
				if window.visible?
					window.hide
				else
					window.show
				end
			end
		}
	#			@bnotification_tray=Gtk::CheckMenuItem.new(_('Afficher les notifications'))
		@bquit_tray.signal_connect("activate"){window.quit}
		@popup_tray=Gtk::Menu.new
		@popup_tray.append(@bplay_tray)
		@popup_tray.append(@bprev_tray)
		@popup_tray.append(@bnext_tray)
		@popup_tray.append(Gtk::SeparatorMenuItem.new)
		@popup_tray.append(@bshow_tray)
	#			@popup_tray.append(@bnotification_tray)
		@popup_tray.append(Gtk::SeparatorMenuItem.new)
		@popup_tray.append(@bquit_tray)
		@popup_tray.show_all
		
		super()
		self.pixbuf=Gdk::Pixbuf.new(File.join(Pdir,'ZiK.svg'))
		self.tooltip=_('Audio player')
		self.signal_connect('activate'){
			if window.visible?
				window.hide
			else
				window.show
			end
		}
		self.signal_connect('popup-menu'){|tray, button, time|
			@show_tray=false
			@bplay_tray.active=@player.playing?
			@bshow_tray.active=window.visible?
			@show_tray=true
			@popup_tray.popup(nil, nil, button, time)
		}
	end
end
