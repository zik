=begin
Copyright 2007-2008 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end
module ReadPlaylist
	def fetch_playlist(string)
		uri=URI.parse(string)
		return (Net::HTTP.get uri)
	end
	
	def read_m3u(file)
		path=[]
		while line=file.gets
			unless line.nil? or line =~ /^\#/  #line begin with #
				line.chomp!
				path.push(line) unless line.empty?
			end
		end
		return path
	end
	
	def read_pls(file)
		path=[]
		line=file.gets
		while line.nil? #Skip blank lines
			line=file.gets
		end
		if line =~ /^\[/ #formated playlist
			while line=file.gets
				if line =~ /^file[0-9]*\=/i #Search for the path file (ignore case)
					line.chomp!
					path.push(line.split('=')[1])
				end
			end
		else
			line.chomp!
			path.push(line) unless line.nil? 
			while line=file.gets
				path.push(line.chomp!) unless line.nil? or line.empty?
			end
		end
		return path
	end
	
	def read_asx(file)
		path=[]
		while (line=file.gets)
			path.push(URI::extract(line)[0]) if line =~ /\<ref/i #if line contain with <ref
		end
		return path
	end
end
