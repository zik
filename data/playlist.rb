=begin
Copyright 2007-2008 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

#See http://www.xspf.org/ http://xspf.rubyforge.org/doc/index.html to store the playlist in a open format
#xspf2m3u exists but need to write the others conversion method?
class Playlist
	include ReadPlaylist

	attr_reader :path
	attr_reader :name , :duration
	attr_reader :store
	def initialize(window,m3u=false)
		@window=window
		@path=[]
		@name=[]
		@duration=[]
		@store=Gtk::ListStore.new(String,TrueClass)
		if m3u
			@file=File.join(Home,'ZiK.m3u')
			add_m3u(@file)
		end
	end
	
	def write(file=@file)###############################message d'erreurs (console et fenêtre)
		begin
		f=File.open(file,'w')
		f.puts '#EXTM3U'
		for i in 0..@path.length-1
			a='#EXTINF:'+@duration[i].to_s+','+@name[i]
			f.puts a
			f.puts @path[i]
		end
		f.close
		puts "#{file} written."
		rescue
		puts "Unable to write #{file}."
		dialog=Gtk::MessageDialog.new(@window, Gtk::Dialog::DESTROY_WITH_PARENT, Gtk::MessageDialog::ERROR, Gtk::MessageDialog::BUTTONS_CLOSE, "Unable to write #{file}.")
		dialog.run
		dialog.destroy
		end
	end
	
	def del_all
		@path.clear
		@name.clear
		@duration.clear
		@store.clear
	end
	
	def add(selection)
		song=[]
		selection.selected_each{|model, path, iter|
			stock=[]#Array to stock selection if it is  a directory
			ext=iter[1]
			if ext.nil?#selection is a directory
				stock.push(iter)
			else#selection is a file
				song.push(iter[2]+'/'+iter[0]+ext)
			end
			stock.each{|iter|#Process the stocked directories
				nb_children=iter.n_children
				fiter=iter.first_child
				ext=fiter[1]
				if ext.nil?
					stock.push(fiter)
				else
					song.push(fiter[2]+'/'+fiter[0]+ext)
				end
				lpath=fiter.path
				(nb_children-1).times{
#Gtk::TreeIter.next do not exists. Next! method cannot be used here
#A Gtk::TreePath is used instead.
					lpath.next!
					liter=model.get_iter(lpath)
					ext=liter[1]
					if ext.nil?
						stock.push(liter)
					else
						song.push(liter[2]+'/'+liter[0]+ext)
					end
					}
				}
			}
	song.sort!.uniq!
	song.each{|file|
		ext=File.extname(file)
		case ext
		when '.m3u'
			add_m3u(file)
		when '.pls'
			add_pls(file)
		else
			read_tag(file)
		end
		}
	end
	
	def del(selection)
		p=[]
		selection.selected_each{|model, path, iter| p.push(path.to_str.to_i)}
		i=0
		p.each{|pi|
			@path.delete_at(pi-i)
			@name.delete_at(pi-i)
			@duration.delete_at(pi-i)
			i+=1
			}
		view
		return p
	end
	
	def import(window)
		dialog = Gtk::FileChooserDialog.new("Importer une liste de lecture", window, Gtk::FileChooser::ACTION_OPEN, nil, [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL], [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
		filter=Gtk::FileFilter.new
		filter.add_pattern('*.m3u');filter.add_pattern('*.pls')
		dialog.filter=filter
		if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
			file=dialog.filename
			ext=File.extname(file)
			case ext
			when '.m3u'
				add_m3u(file)
			when '.pls'
				add_pls(file)
			end
		end
		dialog.destroy
	end
	
	def export(window)
		dialog = Gtk::FileChooserDialog.new("Exporter une liste de lecture", window, Gtk::FileChooser::ACTION_SAVE, nil, [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL], [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
		dialog.show_hidden=false#Don't work??
		dialog.filter=Gtk::FileFilter.new.add_pattern('*.m3u')
		dialog.do_overwrite_confirmation=true
		if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
			fname=dialog.filename
			ext=File.extname(fname)
			if ext==''
				fname+='.m3u'
			end
			write(fname)
		end
		dialog.destroy
	end
	
	def copy(list)
	list.path.each{ |p|
		@path.push(p)
		}
	list.name.each{ |n|
		@name.push(n)
		}
	list.duration.each{ |d|
		@duration.push(d)
		}
	view#########################À supp. Ajouter directement dans @store!
	end
	
	def up(selection)
		num=[]
		itera=[];patha=[]
		selection.selected_each{|model, path, iter|
			itera.push(iter);patha.push(path)
			num.push(path.to_s.to_i)
		}
		test=0
		while num[0]==test
			num.shift;;itera.shift;patha.shift
			test+=1
		end
		num.each{|i|
		@path[i],temp=@path[i-1]=@path[i-1],@path[i]
		@name[i],@name[i-1]=@name[i-1],@name[i]
		@duration[i],@duration[i-1]=@duration[i-1],@duration[i]
		}
		mat=[itera,patha].transpose
		mat.each{|iter,path|
			path.prev!
			iter_prev=@store.get_iter(path)
			@store.move_before(iter,iter_prev)
			}
	end
	
	def down(selection)
		num=[]
		itera=[];patha=[]
		selection.selected_each{|model, path, iter|
			itera.push(iter);patha.push(path)
			num.push(path.to_s.to_i)
		}
		num.reverse!
		itera.reverse!;patha.reverse!
		test=@path.length-1
		while num[0]==test
			num.shift
			itera.shift;patha.shift
			test-=1
		end
		mat=[itera,patha,num].transpose
		mat.each{|iter,path,i|
		@path[i],@path[i+1]=@path[i+1],@path[i]
		@name[i],@name[i+1]=@name[i+1],@name[i]
		@duration[i],@duration[i+1]=@duration[i+1],@duration[i]
		path.next!
		iter_next=@store.get_iter(path)
		@store.move_after(iter,iter_next)
		}
	end
	
	def refresh
		@name.clear; @duration.clear; @store.clear
		@path.each{|file| read_tag(file,false)}
	end
	
	def add_m3u(file)
		begin
		f=File.open(file,'r')
		basedir=File.dirname(file)
		array=read_m3u(f)
		array.each{|line|
			if line =~ /^\//#absolute path
				read_tag(line)
			else#relative path
				read_tag(File.join(basedir,line))
			end
		}
		puts "#{file} read."
		rescue
		puts "Unable to read #{file}."
		ensure
		f.close unless f.nil?
		end
	end
	
	def add_pls(file)
		begin
			f=File.open(file)
			basedir=File.dirname(file)
			array=read_pls(f)
			array.each{|line|
				if line =~ /^\//#absolute path
					read_tag(line)
				else#relative path
					read_tag(File.join(basedir,line))
				end
			}
			puts "#{file} read."
		rescue
			puts "Unable to read #{file}."
		ensure
			f.close	unless f.nil?
		end
	end
	
	def read_tag(file,add=true)
		error=false
		begin
			raise TagLib::BadPath.new unless File.exist?(file)
			tag=TagLib::File.new(file)
			title=tag.title
			artist=tag.artist
			length=tag.length
		rescue TagLib::BadPath
			error=true
			$stderr.puts "Error searching #{file}."
		rescue TagLib::BadFile
			$stderr.puts "Cannot read #{file}\'s tag."
			title='';artist='';length=-1
		rescue TagLib::BadTag
			$stderr.puts "Error reading #{file}\'s tag."
			title='';artist='';length=-1
		ensure
			tag.close unless tag.nil?
		end
		unless error
			name=artist+' - '+title
			if name==' - '
				ext=File.extname(file)
				name=File.basename(file,ext)
			end
			@path.push(file) if add #Do not add the path when refreshing the treeview
			@name.push(name)
			@duration.push(length)
			child=@store.append
			child[0]=name
			child[1]=false
		end
	end
	
	private
	
	def view
		@store.clear
		@name.each{|name|
		child=@store.append
		child[0]=name
		child[1]=false
		}
	end
	
end
