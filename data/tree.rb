=begin
Copyright 2007-2008 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end
#**********************************
#-----Tree explorer----------------
#**********************************

####################Voir /usr/share/doc/libgtk2-ruby/examples/gtk-demo/main.rb

class Tree < Gtk::TreeStore

def refresh(directory,ext)
	printf 'Refreshing treeview...'
	self.clear
	extension=''
	ext.each{|ext,show|
		extension+=ext+',' if show
	}
	extension.chop!
	l=directory.length
	unless directory[0].nil? or extension.empty?
		for i in 0..l-1
			songa=[]
			rpath=directory[i]
			lr=rpath.split('/').length
			songa=Dir.glob(File.join(rpath,"**","*.{"+extension+"}"),File::FNM_CASEFOLD).sort
#Root directory
			root=self.append(nil)
			root[0]=rpath
			dirl=rpath#last directory scanned
			child_rec=[]#keep record of children
			songa.each{|song|
				ext=File.extname(song)
				name=File.basename(song,ext)
				dir=File.dirname(song)
				if dir==rpath
#Songs in root directory
					child= self.append(root)
					child[0]=name
					child[1]=ext
					child[2]=rpath
				else
					dira=[]
					dira=dir.split('/');la=dira.length
					dirla=dirl.split('/')
					unless dira[lr]==dirla[lr]
#sub-directories of root directory
					child=self.append(root)
					child[0]=dira[lr]
					child_rec[0]=child
#					child[2]=dir
					end
					for j in lr+1..la-1
						unless dira[j]==dirla[j]
#all sub-directories of first children directories
							child=self.append(child_rec[j-lr-1])
							child[0]=dira[j]
							child_rec[j-lr]=child
#							child[2]=dir
						end
					end
#Songs in other directories
					child=self.append(child_rec[la-1-lr])
					child[0]=name
					child[1]=ext
					child[2]=dir
				end
				dirl=dir
				}
		end
	end
	puts 'done.'
	return songa
end

end
