=begin
Copyright 2007-2008 Vincent Carmona

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end
include GetText
bindtextdomain('ZiK',{:path=>Ldir})
module Search
	def Search.init
		@@plist=false
		@@explo=false
	end
	
	def Search.plist?
		return @@plist
	end
	
	def Search.explo?
		return @@explo
	end
	
	private
	
	def Search.plist=(tf)
		@@plist=tf
	end
	
	def Search.explo=(tf)
		@@explo=tf
	end
	
	public
	
	class List < Gtk::Window
		def initialize
			@pos=0;@l=0;@result=[]
				
			@entry=Gtk::Entry.new
			@entry.signal_connect("activate"){
				pattern=Regexp.new(@entry.text,true)#equivalent to:	/text/i
				@result=[]#Array to contain treepath
				i=0
				@name.each{|name|
					 @result.push(Gtk::TreePath.new(i.to_s)) unless (name =~ pattern).nil?
					i+=1
				}
				#select the results on the treeview
				@selection.unselect_all
				 @result.each{|r| @selection.select_path(r)}
				#focus to first result
				@l=@result.length;@pos=0
				@view.scroll_to_cell( @result[@pos],nil,true,0.5,0.0)
			}
			@prev=Gtk::EventBox.new.add(Gtk::Label.new(_('Previous')))
			@prev.signal_connect('button-press-event'){|widget,event|
				if event.button==1
					@pos-=1
					@pos=@l-1 if @pos<0#Return to the first result after the last one
					@view.scroll_to_cell( @result[@pos],nil,true,0.5,0.0)
				end
			}
			@next=Gtk::EventBox.new.add(Gtk::Label.new(_('Next')))
			@next.signal_connect('button-press-event'){|widget,event|
				if event.button==1
					@pos+=1
					@pos=0 if @pos>=@l#Return to the first result after the last one
					@view.scroll_to_cell( @result[@pos],nil,true,0.5,0.0)
				end
			}
			
			@table=Gtk::Table.new(2,2,false)
			@table.attach(@entry,0,2,0,1)
			@table.attach(@prev,0,1,1,2,Gtk::SHRINK,Gtk::SHRINK)
			@table.attach(@next,1,2,1,2,Gtk::SHRINK,Gtk::SHRINK)
			@frame=Gtk::Frame.new(_('Find'))
			@frame.add(@table)
			@frame.show_all
			
			super(Gtk::Window::TOPLEVEL)
			self.decorated=false
			self.skip_taskbar_hint=true
			self.skip_pager_hint=true
			self.resizable=false
			self.add(@frame)
			self.signal_connect("focus-out-event"){self.hide}
			Search.plist=true
		end
		
		def search(position,list,treeview)
			self.move(position[0]+position[2],position[1]+position[3])
			self.show
			@name=list.name
			@view=treeview
			@selection=treeview.selection
		end
	end
	
	class Explo < Gtk::Window
		def initialize(player,list1,list2,border=10)
			@player=player
			@list1=list1;@list2=list2
			@refresh=true;@path=[];@result=[];@name=[]
			
			@block_sig=false
			@bpath=Gtk::CheckButton.new(_('Full path'))
			@bpath.active=true
			@bpath.signal_connect("toggled"){
				unless @block_sig
					@block_sig=true
					@bname.active=false if @bpath.active?
					@block_sig=false
				end
			}
			@bname=Gtk::CheckButton.new(_('File name'))
			@bname.active=false
			@bname.signal_connect("toggled"){
				unless @block_sig
					@block_sig=true
					@bpath.active=false if @bname.active?
					@block_sig=false
				end
			}
			
			@btitle=Gtk::CheckButton.new(_('Title'))
			@bartist=Gtk::CheckButton.new(_('Artist'))
			@balbum=Gtk::CheckButton.new(_('Album'))
			@bgenre=Gtk::CheckButton.new(_('Genre'))
			
			@block_sig2=false
			@ball=Gtk::CheckButton.new(_('All files'))
			@ball.active=true
			@ball.signal_connect("toggled"){
				unless @block_sig2
					@block_sig2=true
					@bresult.active=false if @ball.active?
					@block_sig2=false
				end
			}
			@bresult=Gtk::CheckButton.new(_('Only results'))
			@bresult.active=false
			@bresult.signal_connect("toggled"){
				unless @block_sig2
					@block_sig2=true
					@ball.active=false if @bresult.active?
					@block_sig2=false
				end
			}
			
			@table=Gtk::Table.new(4,2)
			@table.attach(@bpath,0,1,0,1)
			@table.attach(@bname,1,2,0,1)
			@table.attach(@btitle,0,1,1,2)
			@table.attach(@bartist,1,2,1,2)
			@table.attach(@balbum,0,1,2,3)
			@table.attach(@bgenre,1,2,2,3)
			@table.attach(@ball,0,1,3,4)
			@table.attach(@bresult,1,2,3,4)
			@frame_opt=Gtk::Frame.new(_('Find options'))
			@frame_opt.add(@table)
			
			@entry=Gtk::Entry.new
			@entry.signal_connect("activate"){
				begin
					pattern=Regexp.new(@entry.text,true)#equivalent to:	/text/i
					#search in previous result
					if @bresult.active?
						bresult=true
						temp=@path
						@path=@result
					else
						tf=false
					end
					@result=[]
					result=[]
					#search in path name
					if @bpath.active?
						i=0
						@path.each{|path|
							result.push(i) unless (path =~ pattern).nil?
							i+=1
						}
					end
					#search in file name
					if @bname.active?
						i=0
						@name.each{|name|
							result.push(i) unless (name =~ pattern).nil?
							i+=1
						}
					end
					#search in tags
					if @btitle.active? or @bartist.active? or @balbum.active? or @bgenre.active?
						if @refresh##--> Realod tag
							@title=[];@artist=[];@album=[];@genre=[]
							@path.each{|path|
								begin
									tag=TagLib::File.new(path)
									begin @title.push(tag.title) rescue @title.push(nil) end
									begin @artist.push(tag.artist) rescue @artist.push(nil) end
									begin @album.push(tag.album) rescue @album.push(nil) end
									begin @genre.push(tag.genre) rescue @genre.push(nil) end
								rescue TagLib::BadFile
									@title.push(nil);@artist.push(nil);@album.push(nil);@genre.push(nil)
								rescue TagLib::BadTag
									@title.push(nil);@artist.push(nil);@album.push(nil);@genre.push(nil)
								ensure
									tag.close unless tag.nil?
								end
							}
							@refresh=false
						end
					end
					if @btitle.active?##--> Search in title.
						i=0
						@title.each{|title|
							unless title.nil?
								result.push(i) unless (title =~ pattern).nil?
							end
								i+=1
						}
					end
					if @bartist.active?##--> Search in artist.
						i=0
						@artist.each{|artist|
							unless artist.nil?
								result.push(i) unless (artist =~ pattern).nil?
							end
							i+=1
							
						}
					end
					if @balbum.active?##--> Search in album.
						i=0
						@album.each{|album|
							unless album.nil?
								result.push(i) unless (album =~ pattern).nil?
							end
							i+=1
						}
					end
					if @bgenre.active?##--> Search in genre.
						i=0
						@genre.each{|genre|
							unless genre.nil?
								result.push(i) unless (genre =~ pattern).nil?
							end
							i+=1
						}
					end
					#collect the results
					@result.clear
					result=result.uniq.sort
					result.each{|i|
						p=@path[i]
						child=@store.append
						child[0]=p
						@result.push(p)
					}
					@path=temp if bresult
				rescue
					$stderr.puts 'Error while searching.'
					dialog=Gtk::MessageDialog.new(self,Gtk::Dialog::DESTROY_WITH_PARENT,
						Gtk::MessageDialog::WARNING,Gtk::MessageDialog::BUTTONS_CLOSE,
						_("Error searching '%s'") % @entry.text)
					dialog.run;dialog.destroy
				end
			}
			
			@store=Gtk::ListStore.new(String)
			@view=Gtk::TreeView.new(@store)
			@view.headers_visible=false
			@select=@view.selection
			@select.mode=Gtk::SELECTION_MULTIPLE
			@renderer=Gtk::CellRendererText.new
			@col=Gtk::TreeViewColumn.new(_("Result"), @renderer, :text => 0)
			@view.append_column(@col)
			@wscrol=Gtk::ScrolledWindow.new
			@wscrol.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
			@wscrol.add(@view)
			
			@add2=Gtk::Button.new(Gtk::Stock::ADD)
			@add2.signal_connect('clicked'){
				@select.selected_each{|model,path,iter|
					ext=File.extname(iter[0])
					case ext
					when '.m3u'
						@list2.add_m3u(iter[0])
					when '.pls'
						@list2.add_pls(iter[0])
					else
						@list2.read_tag(iter[0])
					end
				}
			}
			@frame2=Gtk::Frame.new(_('Secondary list'))
			@frame2.add(@add2)
			@clear=Gtk::Button.new(Gtk::Stock::CLEAR)
			@clear.signal_connect('clicked'){@store.clear}
			@add1=Gtk::Button.new(Gtk::Stock::ADD)
			@add1.signal_connect('clicked'){
				@select.selected_each{|model,path,iter|
					ext=File.extname(iter[0])
					case ext
					when '.m3u'
						@list1.add_m3u(iter[0])
					when '.pls'
						@list1.add_pls(iter[0])
					else
						@list1.read_tag(iter[0])
					end
					@player.add
				}
			}
			@frame1=Gtk::Frame.new(_('Playlist'))
			@frame1.add(@add1)			
			@bbox=Gtk::HBox.new
			@bbox.pack_start(@frame2,true,false)
			@bbox.pack_start(@clear,true,false)
			@bbox.pack_start(@frame1,true,false)
			
			@close=Gtk::Button.new(Gtk::Stock::CLOSE)
			@close.signal_connect('clicked'){self.hide}
			
			@vbox=Gtk::VBox.new(false)
			@vbox.pack_start(@frame_opt,false)
			@vbox.pack_start(@entry,false)
			@vbox.pack_start(@wscrol,true)
			@vbox.pack_start(@bbox,false)
			@vbox.pack_start(@close,false)
			@vbox.show_all
			super(Gtk::Window::TOPLEVEL)
			self.set_default_size(400,500)
			self.title=_('Find')
			self.icon=Gdk::Pixbuf.new(File.join(Pdir,'ZiK.svg'))
			self.add(@vbox)
			self.signal_connect("delete_event"){self.hide;true}
			Search.explo=true
		end
		
		def search(position,path)
			unless path==@path
				@path=path;@name.clear;@refresh=true#Need to reload tags
				@path.each{|path| @name.push(File.basename(path))}
			end
			
			self.move(position[0]+position[2]+40,position[1]+position[3]+40)
			self.show
		end
	end
end
