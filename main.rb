#!/usr/bin/env ruby

=begin
Copyright 2007-2008 Vincent Carmona
vinc-mai@caramail.com

This file is part of ZiK.

    ZiK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ZiK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZiK; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

#Ajouter: sorties en consoles pour savoir ce fait ZiK (+ option quiet?? -> voir optparse)

Pdir='pix_directory' #Pictures path
Ddir='doc_directory' #Documentations path
Ldir='locale_directory' #locale path
Version='0.5.5'

Dir.chdir('data_directory')

require 'gtk2'
require 'gst0.10'
require 'taglib'


require 'gettext'

require 'systray'
require 'tree'
require 'option'
require 'help'
require 'edittag'
require 'search'

require 'uri'
require 'net/http'
require 'tempfile'

require 'readplaylist'

require 'preference'
require 'playlist'
require 'player'
require 'gui'

Dir.chdir(ENV['HOME'])

version=Gtk::BINDING_VERSION
Gtk.init if version[0]==0 and version[1]<15#Gtk.init is call in Gtk.rb since version 0.15.0
Gst.init
Search.init

while ARGV.length!=0
	case ARGV.shift
	when '-d'#Change the path for configuration file and playlist file.
		dir=ARGV.shift;
		unless File.directory?(dir)
			$stderr.puts "#{dir} is not a directory."
			exit 1;
		end
		Home=File.expand_path(dir);
	else
		$stderr.puts "Unknown option"
		exit 1;
	end
end

Home=File.join(ENV['HOME'],'.ZiK') unless (defined? Home)
#---Create ZiK directory ---------
if !File.exist?(Home)
	Dir.mkdir(Home)
end

window=Gui.new
window.show
Gtk.main
